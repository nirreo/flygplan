import java.util.Scanner;


public class Admin { // Admin class h�r g�rs allt i admin menyn.

	static Scanner input = new Scanner(System.in);

	
	public void adminOptions() { // Metod f�r huvud admin menyn.
		MainMenu mainMenu = new MainMenu();

		System.out
				.println("Admin Menu:\n1. Create\n2. Delete\n3. Search\n4. Main menu");
		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				create();
				break;
			case 2:
				delete();
				break;

			case 3:
				search();
				break;

			case 4:
				mainMenu.startMenu();
				break;
			case 5:

				break;
			}
		} while (userIn >= 5);

	}

	
	public void create() { // metod f�r botten menyn som ska "Creata" Airport, Airline, Flight.

		System.out
				.println("Create:\n1. Create Airport\n2. Create Airline\n3. Create Flight\n4. Back");
		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				createAirport();
				break;
			case 2:
				createAirline();
				break;

			case 3:
				createFlight();
				break;

			case 4:
				adminOptions();
				break;
			case 5:

				break;
			}
		} while (userIn >= 5);

	}

	
	public void delete() { // Metod f�r botten menyn som ska navigera genom olika delete funktioner.
		System.out
				.println("Delete:\n1. Delete airport\n2. Delete airline\n3. Delete flight\n4. Back");
		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				deleteAirport();
				break;
			case 2:
				deleteAirline();
				break;

			case 3:
				deleteFlight();
				break;

			case 4:
				adminOptions();
				break;
			case 5:

				break;
			}
		} while (userIn >= 5);
	}

	
	public void search() { // Metod f�r att grundmenyn till s�kfunktionen.
		DatabaseConnector setup = new DatabaseConnector();
		System.out
				.println("Search flight:\n1. Search by departure:\n2. Search by destination\n3. Back");
		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out.print("Enter departure location: ");
				setup.departureSearch();
				System.out.println("\n1. Back");
				backSearch();
				break;
			case 2:
				System.out.print("Enter Destination: ");
				setup.destinationSearch();
				System.out.println("\n1. Back");
				backSearch();
				break;

			case 3:
				adminOptions();
				break;

			}
		} while (userIn >= 4);
	}
	
	public void createAirport() { // Metod med f�r skapandet av Airports i databas
		String airIn;
		String location;
		int userIn;
		DatabaseConnector airportIn = new DatabaseConnector();
		do {
			System.out.println("1. Add\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.println("Create an airport that contains 3 letters: ");

				do {
					airIn = input.next();
					if (!Regex.regexAirport(airIn)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexAirport(airIn));

				System.out.print("Enter airport location: ");
				location = input.next();
				airportIn.airportInsert(airIn, location);
				break;
			case 2:
				create();
				break;
			}

		} while (!(userIn >= 2));

	}
	
	public void createAirline() { // Metod f�r att skapa airlines.
		String airIn;
		int userIn;
		DatabaseConnector airlineIn = new DatabaseConnector();
		do {
			System.out.println("1. Add\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print("Create an airline that contains a minimum of 6 signs: ");

				do {
					airIn = input.next();
					airIn = airIn + input.nextLine();
					if (!Regex.regexAirline(airIn)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexAirline(airIn));

				airlineIn.airlineInsert(airIn);
				break;
			case 2:
				create();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void createFlight() { // Metod f�r att skapa nya flights mellan olika destinationer. 
		
		String departure_airport;
		String departure;
		String destination_airport;
		String destination;
		String date;
		int airlineIn;
		int userIn;
		int departureIn;
		DatabaseConnector flightIn = new DatabaseConnector();
		DatabaseConnector showAirline = new DatabaseConnector();
		DatabaseConnector departureIN = new DatabaseConnector();
		DatabaseConnector showAirport = new DatabaseConnector();
		do {
			System.out.println("1. New flight\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				showAirline.showAirline();
				System.out.print("Pick Airline: ");
				airlineIn = input.nextInt();
				
				/*showAirport.showAirport();
				System.out.println("Pick destination airport:");
				departureIn = input.nextInt();
				departureIN.departureIn(departureIn);*/
				
				System.out.print("Set the 3 letter departure airport code: ");
				do {
					departure_airport = input.next();
					if (!Regex.regexAirport(departure_airport)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexAirport(departure_airport));
				System.out.print("Set departure airport location: ");
				do {
					departure = input.next();
					departure = departure + input.nextLine();
					if (!Regex.regexDestination(departure)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexDestination(departure));
				
				System.out
						.print("Set the 3 letter destination airport code: ");
				do {
					destination_airport = input.next();
					if (!Regex.regexAirport(destination_airport)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexAirport(destination_airport));
				System.out.print("Set destination To: ");
				do {
					destination = input.next();
					destination = destination + input.nextLine();
					if (!Regex.regexDestination(destination)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexDestination(destination));
				System.out.print("Set date (yyyy-mm-dd): ");
				do {
					date = input.next();
					if (!Regex.regexDate(date)) {
						System.out.println("Invalid input please try again");
					}
				} while (!Regex.regexDate(date));

				flightIn.flightInsert(departure_airport, departure,
						destination_airport, destination, date, airlineIn);
				
				break;
			case 2:
				create();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void deleteAirport() { // Metod f�r f�r meny till deletfuntioner av airports.
		int userIn;
		DatabaseConnector showAirport = new DatabaseConnector();
		DatabaseConnector delAirport = new DatabaseConnector();

		do {
			showAirport.showAirport();
			System.out.println("\n1. delete\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print("Delete an airport\nType the number of the airport you want to delete: ");
				delAirport.delAirport();
				System.out.println("Deleted!");
				break;
			case 2:
				delete();
				break;
			}

		} while (!(userIn >= 2));

	}
	
	public void deleteAirline() { // metod f�r meny val av delete airlines
		int userIn;
		DatabaseConnector showAirline = new DatabaseConnector();
		DatabaseConnector delAirline = new DatabaseConnector();

		do {
			showAirline.showAirline();
			System.out.println("\n1. delete\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print ("Delete an airline\nType the number of the airline you want to delete: ");
				delAirline.delAirline();
				System.out.println("Deleted!");
				break;
			case 2:
				delete();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void deleteFlight() { // Metod f�r delete menyn med val av Flights
		String delIn;
		int userIn;
		DatabaseConnector showFlight = new DatabaseConnector();
		DatabaseConnector delFlight = new DatabaseConnector();

		do {
			showFlight.showFlight();
			System.out.println("\n1. delete\n2. Back");
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.print("Delete an flight\nType the number of the flight you want to delete: ");
				delFlight.delFlight();
				System.out.println("Deleted!");
				break;
			case 2:
				delete();
				break;
			}

		} while (!(userIn >= 2));
	}
	
	public void backSearch() { // Metod som har funktion f�r att backa i en av s�kmenyerna.

		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				search();
				break;

			}
		} while (userIn >= 1);

	}
}